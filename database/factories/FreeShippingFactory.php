<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Database\Factories;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Database\Factories\ShippingZoneFactory;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<FreeShipping>
 */
final class FreeShippingFactory extends Factory
{
    /**
     * @var class-string<FreeShipping>
     */
    protected $model = FreeShipping::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'minimum_order_amount' => new Price($this->faker->numberBetween(0, 50)),
            'active' => true,
            'shipping_zone_id' => (new ShippingZoneFactory())->create(),
        ];
    }
}
