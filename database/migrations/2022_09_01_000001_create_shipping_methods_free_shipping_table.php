<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('shipping_methods_free_shipping', static function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('shipping_zone_id');
            $table->string('name');
            $table->integer('minimum_order_amount')->nullable()
                ->comment('Cantidad mínima del carrito para aplicar el envío gratis');
            $table->boolean('active')->default(true);
            $table->timestamps();

            $table->foreign('shipping_zone_id')->references('id')->on('shipping_zones');
        });
    }

    public function down(): void
    {
        Schema::drop('shipping_methods_free_shipping');
    }
};
