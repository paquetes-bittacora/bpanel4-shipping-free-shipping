<?php

declare(strict_types=1);

return [
    'bpanel4-free-shipping' => 'Envío gratis',
    'bpanel' => 'Editar',
];
