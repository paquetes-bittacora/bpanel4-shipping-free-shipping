<?php

declare(strict_types=1);

return [
    'create-free-shipping' => 'Crear gastos de envío gratis',
    'edit-free-shipping' => 'Editar gastos de envío gratis',
    'name' => 'Nombre',
    'minimum_order_amount' => 'Pedido mínimo para envío gratis',
];
