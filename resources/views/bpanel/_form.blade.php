@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-free-shipping::form.new-product'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ $panelTitle }}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{ $action }}" enctype="multipart/form-data">
            @csrf
            @livewire('form::input-text', ['name' => 'name', 'labelText' => __('bpanel4-free-shipping::form.name'), 'required'=>true, 'value' => old('name') ?? $model?->getName() ])
            @livewire('form::input-number', [
                'name' => 'minimum_order_amount',
                'labelText' => __('bpanel4-free-shipping::form.minimum_order_amount'),
                'required'=>true,
                'value' => old('minimum_order_amount') ?? $model?->getMinimumOrderAmount()->toFloat() ?? 0,
                'min' => 0,
                'step' => 0.01,
                'fieldWidth' => 7,
                'labelWidth' => 3
            ])
            @livewire('form::input-checkbox', [
                'name' => 'active',
                'value' => 1,
                'checked' => $model?->isActive() ?? false,
                'labelText' => __('bpanel4-shipping::form.active'), 'bpanelForm' => true
            ])
            <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
                @livewire('form::save-button',['theme'=>'update'])
                @livewire('form::save-button',['theme'=>'reset'])
            </div>
            @if (null !== $model)
                <input type="hidden" name="id" value="{{ $model->getId() }}">
                <input type="hidden" name="shipping_zone_id" value="{{ $model->getShippingZoneId() }}">
            @else
                <input type="hidden" name="shipping_zone_id" value="{{ $zone->getId() }}">
            @endif
        </form>
        @if($errors->any())
            {{ implode('', $errors->all('<div>:message</div>')) }}
        @endif
    </div>

@endsection
