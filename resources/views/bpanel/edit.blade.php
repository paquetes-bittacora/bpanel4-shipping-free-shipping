@extends('bpanel4::layouts.bpanel-app')

@section('title', __('bpanel4-free-shipping::form.edit-free-shipping'))

@section('content')
    <div class="card bcard">
        <div class="card-header bgc-primary-d1 text-white border-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('bpanel4-free-shipping::form.edit-free-shipping') }}</span>
            </h4>
        </div>
        <form class="mt-lg-3" autocomplete="off" method="post" action="{{route('bpanel4-shipping.free-shipping.bpanel.update', [
            'model' => $model,
        ])}}">
            @include('bpanel4-free-shipping::bpanel._form', ['panelTitle' => __('bpanel4-free-shipping::form.edit-free-shipping')])
        </form>
    </div>

@endsection
