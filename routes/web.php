<?php

declare(strict_types=1);

// Métodos de envío
use Bittacora\Bpanel4\Shipping\FreeShipping\Http\Controllers\FreeShippingAdminController;
use Illuminate\Support\Facades\Route;

Route::prefix('bpanel/envio')->name('bpanel4-shipping.free-shipping.bpanel.')->middleware(['web', 'auth', 'admin-menu'])
    ->group(static function () {
        Route::get('/metodos-de-envio/FreeShipping/{zone}/crear', [FreeShippingAdminController::class, 'create'])->name('create');
        Route::post('/metodos-de-envio/FreeShipping/{zone}/crear', [FreeShippingAdminController::class, 'store'])->name('store');
        Route::get('/metodos-de-envio/FreeShipping/{model}/editar', [FreeShippingAdminController::class, 'edit'])->name('edit');
        Route::post('/metodos-de-envio/FreeShipping/{model}/editar', [FreeShippingAdminController::class, 'update'])->name('update');
        Route::delete('/metodos-de-envio/FreeShipping/{model}/eliminar', [FreeShippingAdminController::class, 'destroy'])->name('destroy');
    });
