<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Tests\Unit\MockFactories;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use Mockery;
use Mockery\MockInterface;

final class FreeShippingMockFactory
{
    private MockInterface&FreeShipping $mock;

    public function __construct()
    {
        $this->mock = Mockery::mock(FreeShipping::class);
    }

    public function getMock(): MockInterface&FreeShipping
    {
        return $this->mock;
    }

    /**
     * @throws InvalidPriceException
     */
    public function withMinimumOrderAmount(int $minimum): self
    {
        $this->mock->shouldReceive('getMinimumOrderAmount')->andReturn(new Price((float)$minimum));
        return $this;
    }

    public static function new(): self
    {
        return new self();
    }
}
