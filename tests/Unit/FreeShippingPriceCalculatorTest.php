<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Tests\Unit;

use Bittacora\Bpanel4\Orders\Tests\Factories\CartMockFactory;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Shipping\FreeShipping\Tests\Unit\MockFactories\FreeShippingMockFactory;
use Bittacora\Bpanel4\Shipping\Services\PriceCalculators\FreeShippingPriceCalculator;
use Tests\TestCase;

final class FreeShippingPriceCalculatorTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     * @throws InvalidPriceException
     */
    public function testSoloEstaHabilitadoSiSeSuperaElPrecioMinimo(int $subtotal, bool $result): void
    {
        $priceCalculator = new FreeShippingPriceCalculator(
            FreeShippingMockFactory::new()->withMinimumOrderAmount(100)->getMock(),
            CartMockFactory::new()->withSubtotalWithTaxes($subtotal)->getMock(),
        );

        self::assertEqualsCanonicalizing($result, $priceCalculator->isEnabled());
    }

    /**
     * @return array<int, array<int, bool|int>>
     */
    public static function dataProvider(): array
    {
        return [
            [0, false],
            [10, false],
            [100, true],
            [1000, true],
        ];
    }
}
