<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Tests\Feature;

use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Database\Factories\ShippingZoneFactory;
use Bittacora\Bpanel4\Shipping\FreeShipping\Actions\CreateFreeShipping;
use Bittacora\Bpanel4\Shipping\FreeShipping\Dtos\CreateFreeShippingDto;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class CreateFreeShippingTest extends TestCase
{
    use RefreshDatabase;

    private CreateFreeShipping $createFreeShipping;

    public function setUp(): void
    {
        parent::setUp();
        $this->createFreeShipping = $this->app->make(CreateFreeShipping::class);
    }

    /**
     * @throws Exception
     */
    public function testCreaUnMetodoDeEnvio(): void
    {
        // Arrange
        $name = 'Envío gratis ' . microtime();
        $amount = new Price(random_int(0, 99999));
        $shippingZone = (new ShippingZoneFactory())->create();
        $dto = new CreateFreeShippingDto($shippingZone->getId(), $name, $amount, true);

        // Act
        $this->createFreeShipping->handle($dto);

        // Assert
        self::assertDatabaseHas('shipping_methods_free_shipping', [
            'name' => $name,
            'minimum_order_amount' => $amount->toInt(),
            'shipping_zone_id' => $shippingZone->getId(),
        ]);
    }
}
