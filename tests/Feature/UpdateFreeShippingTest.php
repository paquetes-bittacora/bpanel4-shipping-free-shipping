<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Tests\Feature;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\FreeShipping\Actions\UpdateFreeShipping;
use Bittacora\Bpanel4\Shipping\FreeShipping\Database\Factories\FreeShippingFactory;
use Bittacora\Bpanel4\Shipping\FreeShipping\Dtos\UpdateFreeShippingDto;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class UpdateFreeShippingTest extends TestCase
{
    use RefreshDatabase;

    private UpdateFreeShipping $updateFreeShipping;

    public function setUp(): void
    {
        parent::setUp();

        $this->updateFreeShipping = $this->app->make(UpdateFreeShipping::class);
    }

    /**
     * @throws InvalidPriceException
     */
    public function testActualizaUnEnvioGratis(): void
    {
        (new FreeShippingFactory())->create();

        $this->updateFreeShipping->handle(new UpdateFreeShippingDto(
            1,
            1,
            'Nombre modificado',
            new Price(1),
            true
        ));

        $updatedModel = FreeShipping::whereId(1)->firstOrFail();
        self::assertEquals('Nombre modificado', $updatedModel->getName());
        self::assertEquals(10000, $updatedModel->getMinimumOrderAmount()->toInt());
    }
}
