<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping;

use Bittacora\Bpanel4\Shipping\FreeShipping\Commands\InstallCommand;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use Bittacora\Bpanel4\Shipping\ShippingFacade;
use Illuminate\Support\ServiceProvider;

final class FreeShippingServiceProvider extends ServiceProvider
{
    private const PACKAGE_PREFIX = 'bpanel4-free-shipping';

    public function boot(): void
    {
        $this->commands(InstallCommand::class);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', self::PACKAGE_PREFIX);
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', self::PACKAGE_PREFIX);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        ShippingFacade::registerShippingMethod('Envío gratis', FreeShipping::class);
    }
}
