<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Validation;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Validation\ValidationException;

final class FreeShippingValidator
{
    public function __construct(private readonly Factory $validator)
    {
    }

    /**
     * @return array<string, string>
     */
    public function getFreeShippingCreationValidationFields(): array
    {
        return [
            'name' => 'required|max:250',
            'shipping_zone_id' => 'required|exists:shipping_zones,id',
            'active' => 'sometimes',
            'minimum_order_amount' => 'required',
        ];
    }

    /**
     * @return array<string, string>
     */
    public function getFreeShippingUpdateValidationFields(): array
    {
        return $this->getFreeShippingCreationValidationFields() + [
                'id' => 'required|numeric',
            ];
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function validateUpdateFreeShipping(array $data): void
    {
        $this->validator->validate($data, $this->getFreeShippingUpdateValidationFields());
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function validateFreeShippingCreationValidation(array $data): void
    {
        $this->validator->validate($data, $this->getFreeShippingCreationValidationFields());
    }
}
