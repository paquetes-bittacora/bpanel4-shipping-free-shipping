<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Dtos;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Support\Dtos\Dto;

final class CreateFreeShippingDto implements Dto
{
    public function __construct(
        public readonly int $shipping_zone_id,
        public readonly string $name,
        public readonly Price $minimum_order_amount,
        public readonly bool $active,
    ) {
    }

    /**
     * @throws InvalidPriceException
     */
    public static function map(array $data): Dto
    {
        return new self(
            (int) $data['shipping_zone_id'],
            $data['name'],
            new Price((float) $data['minimum_order_amount']),
            isset($data['active']) && (bool) $data['active']
        );
    }
}
