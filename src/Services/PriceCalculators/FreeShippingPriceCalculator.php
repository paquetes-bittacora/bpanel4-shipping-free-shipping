<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Services\PriceCalculators;

use Bittacora\Bpanel4\Orders\Models\Cart\Cart;
use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethodPriceCalculator;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use RuntimeException;

final class FreeShippingPriceCalculator implements ShippingMethodPriceCalculator
{
    /**
     * @noRector
     */
    public function __construct(
        private readonly ShippingMethod $shippingMethod,
        private readonly Cart $cart
    ) {
    }

    public function getOptionText(): string
    {
        return 'Envío gratis';
    }

    /**
     * @throws InvalidPriceException
     */
    public function isEnabled(): bool
    {
        if (!$this->shippingMethod instanceof FreeShipping) {
            throw new RuntimeException('El método de envío debe ser FreeShipping');
        }

        return $this->cart->getSubtotalWithTaxes() >= $this->shippingMethod->getMinimumOrderAmount();
    }

    /**
     * @throws InvalidPriceException
     */
    public function getShippingCosts(): ?Price
    {
        return new Price(0.0);
    }
}
