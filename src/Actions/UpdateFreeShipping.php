<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Actions;

use Bittacora\Bpanel4\Shipping\FreeShipping\Dtos\UpdateFreeShippingDto;
use Bittacora\Bpanel4\Shipping\FreeShipping\Validation\FreeShippingValidator;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;

final class UpdateFreeShipping
{
    public function __construct(private readonly FreeShippingValidator $validator)
    {
    }

    public function handle(UpdateFreeShippingDto $dto): void
    {
        $this->validator->validateUpdateFreeShipping((array) $dto);

        $freeShipping = FreeShipping::whereId($dto->id)->firstOrFail();

        $freeShipping->setName($dto->name);
        $freeShipping->setMinimumOrderAmount($dto->minimum_order_amount);
        $freeShipping->setActive($dto->active);

        $freeShipping->save();
    }
}
