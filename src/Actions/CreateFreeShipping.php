<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Actions;

use Bittacora\Bpanel4\Shipping\FreeShipping\Dtos\CreateFreeShippingDto;
use Bittacora\Bpanel4\Shipping\FreeShipping\Validation\FreeShippingValidator;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use Illuminate\Validation\ValidationException;

final class CreateFreeShipping
{
    public function __construct(private readonly FreeShippingValidator $validator)
    {
    }

    /**
     * @throws ValidationException
     */
    public function handle(CreateFreeShippingDto $dto): void
    {
        $this->validator->validateFreeShippingCreationValidation((array) $dto);
        $freeShipping = new FreeShipping();

        $freeShipping->setShippingZoneId($dto->shipping_zone_id);
        $freeShipping->setName($dto->name);
        $freeShipping->setMinimumOrderAmount($dto->minimum_order_amount);
        $freeShipping->setActive($dto->active);

        $freeShipping->save();
    }
}
