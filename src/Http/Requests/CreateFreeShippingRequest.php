<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Http\Requests;

use Bittacora\Bpanel4\Shipping\FreeShipping\Validation\FreeShippingValidator;
use Illuminate\Foundation\Http\FormRequest;

final class CreateFreeShippingRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, string>
     */
    public function rules(FreeShippingValidator $validator): array
    {
        return $validator->getFreeShippingCreationValidationFields();
    }
}
