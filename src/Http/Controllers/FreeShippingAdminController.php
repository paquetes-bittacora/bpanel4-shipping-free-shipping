<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\FreeShipping\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Shipping\FreeShipping\Actions\CreateFreeShipping;
use Bittacora\Bpanel4\Shipping\FreeShipping\Actions\UpdateFreeShipping;
use Bittacora\Bpanel4\Shipping\FreeShipping\Dtos\CreateFreeShippingDto;
use Bittacora\Bpanel4\Shipping\FreeShipping\Dtos\UpdateFreeShippingDto;
use Bittacora\Bpanel4\Shipping\FreeShipping\Http\Requests\CreateFreeShippingRequest;
use Bittacora\Bpanel4\Shipping\FreeShipping\Http\Requests\UpdateFreeShippingRequest;
use Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping;
use Bittacora\Bpanel4\Shipping\Models\ShippingZone;
use Bittacora\Support\Dtos\Exceptions\InvalidDtoClassException;
use Bittacora\Support\Dtos\RequestDtoBuilder;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

final class FreeShippingAdminController extends Controller
{
    public function __construct(
        private readonly Factory $view,
        private readonly Redirector $redirector,
    ) {
    }

    public function create(ShippingZone $zone): View
    {
        $this->authorize('bpanel4-shipping.free-shipping.bpanel.create');
        return $this->view->make('bpanel4-free-shipping::bpanel.create', [
            'zone' => $zone,
            'model' => null,
            'action' => null,
        ]);
    }

    /**
     * @throws InvalidDtoClassException
     */
    public function store(CreateFreeShippingRequest $request, CreateFreeShipping $createFreeShipping): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.free-shipping.bpanel.store');
        $dto = (new RequestDtoBuilder($request, CreateFreeShippingDto::class))->toDto();

        $createFreeShipping->handle($dto);

        return $this->redirector->route('bpanel4-shipping.bpanel.zones.edit', ['zone' => $dto->shipping_zone_id])
            ->with('alert-success', 'Envío gratis creado');
    }

    public function edit(FreeShipping $model): View
    {
        $this->authorize('bpanel4-shipping.free-shipping.bpanel.edit');

        return $this->view->make('bpanel4-free-shipping::bpanel.edit', [
            'model' => $model,
            'action' => null,
        ]);
    }

    /**
     * @throws InvalidDtoClassException
     */
    public function update(
        UpdateFreeShippingRequest $request,
        UpdateFreeShipping $updateFreeShipping,
    ): RedirectResponse {
        $this->authorize('bpanel4-shipping.free-shipping.bpanel.update');

        $dto = (new RequestDtoBuilder($request, UpdateFreeShippingDto::class))->toDto();

        $updateFreeShipping->handle($dto);

        return $this->redirector->route('bpanel4-shipping.bpanel.zones.edit', ['zone' => $dto->shipping_zone_id])
            ->with('alert-success', 'Envío gratis actualizado');
    }

    public function destroy(FreeShipping $model): RedirectResponse
    {
        $this->authorize('bpanel4-shipping.free-shipping.bpanel.destroy');

        $model->delete();

        return $this->redirector->route('bpanel4-shipping.bpanel.zones.edit', ['zone' => $model->getShippingZoneId()])
            ->with('alert-success', 'Envío gratis eliminado');
    }
}
