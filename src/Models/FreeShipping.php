<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Shipping\Models\ShippingMethods;

use Bittacora\Bpanel4\Prices\Casts\PriceCast;
use Bittacora\Bpanel4\Prices\Types\Price;
use Bittacora\Bpanel4\Shipping\Contracts\ShippingMethod;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Bittacora\Bpanel4\Shipping\Traits\HasShippingClasses;

/**
 * Bittacora\Bpanel4\Shipping\Models\ShippingMethods\FreeShipping
 *
 * @property int $id
 * @property int $shipping_zone_id
 * @property string $name
 * @property bool $active
 * @property Price|null $minimum_order_amount Cantidad mínima del carrito para aplicar el envío gratis
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|FreeShipping newModelQuery()
 * @method static Builder|FreeShipping newQuery()
 * @method static Builder|FreeShipping query()
 * @method static Builder|FreeShipping whereCreatedAt($value)
 * @method static Builder|FreeShipping whereId($value)
 * @method static Builder|FreeShipping whereMinimumOrderAmount($value)
 * @method static Builder|FreeShipping whereName($value)
 * @method static Builder|FreeShipping whereShippingZoneId($value)
 * @method static Builder|FreeShipping whereUpdatedAt($value)
 * @mixin Eloquent
 */
final class FreeShipping extends Model implements ShippingMethod
{
    use HasShippingClasses;

    /** @var string $table */
    public $table = 'shipping_methods_free_shipping';

    /**
     * @var array<string, string|class-string<PriceCast>>
     */
    protected $casts = [
        'id' => 'int',
        'active' => 'bool',
        'minimum_order_amount' => PriceCast::class,
    ];

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setMinimumOrderAmount(?Price $amount): void
    {
        $this->minimum_order_amount = $amount;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getMinimumOrderAmount(): ?Price
    {
        return $this->minimum_order_amount;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getShippingZoneId(): int
    {
        return $this->shipping_zone_id;
    }

    public function setShippingZoneId(int $zoneId): void
    {
        $this->shipping_zone_id = $zoneId;
    }
}
